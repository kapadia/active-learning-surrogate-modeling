#!/usr/bin/env python
# coding: utf-8

"""
MIT License

Copyright (c) 2023 Harshit Kapadia

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 
Author: Harshit Kapadia
"""


# --------------------------------------------------------------------------
# Import necessary modules
# --------------------------------------------------------------------------
import sys
import os.path
import pathlib
import warnings

warnings.filterwarnings("ignore")
import numpy as np
import scipy.linalg as spla
from scipy.interpolate import Rbf
from matplotlib import pyplot as plt
from matplotlib import cm
import matplotlib.colors as colors
import scripts.functions as func


# --------------------------------------------------------------------------
# Setup for Burgers' equation
# --------------------------------------------------------------------------
Re = 1000

N = 150
x = np.linspace(0.0, 1.0, N)
dx = 1.0 / np.shape(x)[0]

T = 2.0
num_steps = 100
dt = T / num_steps
tsteps = np.linspace(dt, T, num=num_steps)

def burgers_sol(t, Re):
    kappa = np.exp(Re / 8.0)
    sol = (x / (t + 1)) / (
        1.0 + np.sqrt((t + 1) / kappa) * np.exp(Re * (x * x) / (4.0 * t + 4))
    )
    return sol

def burgers_snap_matrix(Re):
    snap_matrix = np.zeros(shape=(np.shape(x)[0], np.shape(tsteps)[0]))
    for t in np.arange(np.shape(tsteps)[0]):
        snap_matrix[:, t] = burgers_sol(tsteps[t], Re)[:]
    return snap_matrix


# --------------------------------------------------------------------------
# Prepare parameter set
# --------------------------------------------------------------------------

parameterSet = 1 / np.power(10, np.linspace(np.log10(1 / 5500), np.log10(1 / 10), 100))

energy_criteria = 1e-4

snapshots = {}

# Generate solution for initial parameter set
initial_idx = [
    0,
    -1,
    10,
    20,
    30,
    40,
    50,
    60,
    70,
    80,
    90,
    5,
    15,
    25,
    35,
    45,
    55,
    65,
    75,
    85,
    95,
]
for i in initial_idx:
    snapshots[parameterSet[i]] = burgers_snap_matrix(parameterSet[i])


# --------------------------------------------------------------------------
# Active learning
# --------------------------------------------------------------------------

parameterSetLeft = np.delete(parameterSet, initial_idx)

num_steps_coarse = num_steps

dt_coarse = dt

timeGrid = tsteps

reduced = {}
podmodes = {}
svals = {}
poddim = {}

for key in snapshots:
    (
        reduced[key],
        _,
        poddim[key],
        podmodes[key],
        svals[key],
    ) = func.reduced_solution_auto(snapshots[key], energy_criteria)

l2 = {}
for key in snapshots:
    l2[key] = func.norm_l2_rel(snapshots[key], reduced[key], rel=1)

training_l2 = np.log10(np.array(list(l2.values())))
parameter_space = np.array(list(l2.keys()))

l2_interp = []
for n in range(num_steps_coarse):
    rbf_l2 = Rbf(
        parameter_space, training_l2[:, n], epsilon=0.01, function="multiquadric"
    )
    l2_interp.append(rbf_l2(parameterSetLeft))

l2_interp = np.array(l2_interp).T

l2_interp = 10**l2_interp

maxError_p = np.max(l2_interp, axis=1)

maxError = maxError_p.max()

# --------------------------------------------------------------------------

l2_current = np.array(list(l2.values()))
maxError_p_current = np.max(l2_current, axis=1)
maxError_current = maxError_p_current.max()
maxParameterCurrent = list(l2.keys())[maxError_p_current.argmax()]

if maxError_current > maxError:
    while maxError_current > maxError and poddim[maxParameterCurrent] < num_steps:
        poddim[maxParameterCurrent] = poddim[maxParameterCurrent] + 1
        reduced[maxParameterCurrent], _ = func.reduced_sol_recompute(
            snapshots[maxParameterCurrent],
            podmodes[maxParameterCurrent],
            poddim[maxParameterCurrent],
        )

        l2[maxParameterCurrent] = func.norm_l2_rel(
            snapshots[maxParameterCurrent], reduced[maxParameterCurrent], rel=1
        )
        maxError_current = l2[maxParameterCurrent].max()

    training_l2 = np.log10(np.array(list(l2.values())))
    parameter_space = np.array(list(l2.keys()))

    l2_interp = []
    for n in range(num_steps_coarse):
        rbf_l2 = Rbf(
            parameter_space, training_l2[:, n], epsilon=0.01, function="multiquadric"
        )
        l2_interp.append(rbf_l2(parameterSetLeft))

    l2_interp = np.array(l2_interp).T

    l2_interp = 10**l2_interp

    l2_current = np.array(list(l2.values()))
    maxError_p_current = np.max(l2_current, axis=1)

    maxError_current = maxError_p_current.max()
    maxParameterCurrent = list(l2.keys())[maxError_p_current.argmax()]

    maxError_p = np.max(l2_interp, axis=1)

    maxError = maxError_p.max()

# --------------------------------------------------------------------------

parameterAdditional = parameterSetLeft[maxError_p.argmax()]

parameterSetLeft = np.delete(parameterSetLeft, maxError_p.argmax())

estimator = []

while maxError > 1e-2:

    snapshots[parameterAdditional] = burgers_snap_matrix(parameterAdditional)

    # --------------------------------------------------------------------------
    (
        reduced[parameterAdditional],
        _,
        poddim[parameterAdditional],
        podmodes[parameterAdditional],
        svals[parameterAdditional],
    ) = func.reduced_solution_auto(snapshots[parameterAdditional], energy_criteria)

    l2[parameterAdditional] = func.norm_l2_rel(
        snapshots[parameterAdditional], reduced[parameterAdditional], rel=1
    )

    if parameterSetLeft.shape[-1] == 0:
        parameter_space = np.array(list(l2.keys()))
        break
    else:
        training_l2 = np.log10(np.array(list(l2.values())))
        parameter_space = np.array(list(l2.keys()))

        l2_interp = []
        for n in range(num_steps_coarse):
            rbf_l2 = Rbf(
                parameter_space,
                training_l2[:, n],
                epsilon=0.01,
                function="multiquadric",
            )
            l2_interp.append(rbf_l2(parameterSetLeft))

        l2_interp = np.array(l2_interp).T

        l2_interp = 10**l2_interp

        maxError_p = np.max(l2_interp, axis=1)

        maxError = maxError_p.max()

        l2_current = np.array(list(l2.values()))
        maxError_p_current = np.max(l2_current, axis=1)
        maxError_current = maxError_p_current.max()
        maxParameterCurrent = list(l2.keys())[maxError_p_current.argmax()]

        if maxError_current > maxError:

            while (
                maxError_current > maxError and poddim[maxParameterCurrent] < num_steps
            ):
                poddim[maxParameterCurrent] = poddim[maxParameterCurrent] + 1

                reduced[maxParameterCurrent], _ = func.reduced_sol_recompute(
                    snapshots[maxParameterCurrent],
                    podmodes[maxParameterCurrent],
                    poddim[maxParameterCurrent],
                )

                l2[maxParameterCurrent] = func.norm_l2_rel(
                    snapshots[maxParameterCurrent], reduced[maxParameterCurrent], rel=1
                )
                maxError_current = l2[maxParameterCurrent].max()

            training_l2 = np.log10(np.array(list(l2.values())))
            parameter_space = np.array(list(l2.keys()))

            l2_interp = []
            for n in range(num_steps_coarse):
                rbf_l2 = Rbf(
                    parameter_space,
                    training_l2[:, n],
                    epsilon=0.01,
                    function="multiquadric",
                )
                l2_interp.append(rbf_l2(parameterSetLeft))

            l2_interp = np.array(l2_interp).T

            l2_interp = 10**l2_interp

            l2_current = np.array(list(l2.values()))
            maxError_p_current = np.max(l2_current, axis=1)
            maxError_current = maxError_p_current.max()
            maxParameterCurrent = list(l2.keys())[maxError_p_current.argmax()]

            maxError_p = np.max(l2_interp, axis=1)
            maxError = maxError_p.max()

        parameterAdditional = parameterSetLeft[maxError_p.argmax()]

        parameterSetLeft = np.delete(parameterSetLeft, maxError_p.argmax())

        estimator.append(maxError)

# --------------------------------------------------------------------------
estimator = np.array(estimator)


# --------------------------------------------------------------------------
# Error estimate plot
# --------------------------------------------------------------------------

fig, ax = plt.subplots(figsize=(7, 5), nrows=1, ncols=1)
ax.semilogy(estimator[:], "-ro", markersize=7, linewidth=2)
plt.xlabel("Iteration")
plt.ylabel("Error estimate")
plt.tight_layout()
fig.savefig("plots/burgers/Figure5.2a.png", dpi=200)


# --------------------------------------------------------------------------
# Plot selected parameters and their POD dimensions
# --------------------------------------------------------------------------

fig, ax = plt.subplots(figsize=(7, 5), nrows=1, ncols=1)
ax.semilogx(
    1 / np.array(list(poddim.keys())),
    np.array(list(poddim.values())),
    "rx",
    markersize=7,
)
plt.xlabel("Viscosity ($1/Re$)")
plt.ylabel("POD dimension")
plt.ylim((0, 70))
ax.set_yticks([20, 40, 60])
ax.set_xticks([1e-4, 1e-3, 1e-2, 1e-1])
ax.tick_params(axis="x", which="major", pad=7)
plt.tight_layout()
fig.savefig("plots/burgers/Figure5.2b.png", dpi=200)


# --------------------------------------------------------------------------
# Update energy criterion
# --------------------------------------------------------------------------

energy_all = []
for key in poddim:
    TEMP = func.energy_pod(svals[key], poddim[key])
    energy_all.append(TEMP)

min_energy_criteria = 1.0 - np.array(energy_all).max()


# --------------------------------------------------------------------------
# Error Estimate for ROM Solution at Specific Parameter
# --------------------------------------------------------------------------

parameterNew_SET = [3000, 1250, 350, 100, 40]

for parameterNew in parameterNew_SET:

    pathlib.Path(f"plots/burgers/Re{parameterNew}").mkdir(parents=True, exist_ok=True)

    T = 2.0
    num_steps = 100
    dt = T / num_steps
    tsteps = np.linspace(dt, T, num=num_steps)

    training_l2 = np.log10(np.array(list(l2.values())))
    parameter_space = np.array(list(l2.keys()))

    l2_interp = []
    for n in range(num_steps):
        rbf_l2 = Rbf(
            parameter_space, training_l2[:, n], epsilon=0.01, function="multiquadric"
        )
        l2_interp.append(rbf_l2(parameterNew))

    l2_interp = np.array(l2_interp).T

    l2_interp = 10**l2_interp

    np.save(
        f"plots/burgers/Re{parameterNew}/error-estimate-values-original-time-Re",
        l2_interp,
    )


# --------------------------------------------------------------------------
# POD-KSNN surrogate
# --------------------------------------------------------------------------

T = 2.0
num_steps = 100
dt = T / num_steps
tsteps = np.linspace(dt, T, num=num_steps)

snapshots_local = snapshots

# Offline phase
training_snapshots = np.array(list(snapshots_local.values()))
parameter_space = np.array(list(snapshots_local.keys()))

rbf_snapshots = [None] * num_steps_coarse
for n in range(num_steps_coarse):
    rbf_snapshots[n] = Rbf(
        parameter_space,
        training_snapshots[:, :, n],
        function="multiquadric",
        epsilon=0.01,
        mode="N-D",
    )

# Online phase
parameterNew_SET = [3000, 1250, 350, 100, 40]

for parameterNew in parameterNew_SET:

    pathlib.Path(f"plots/burgers/Re{parameterNew}").mkdir(parents=True, exist_ok=True)

    T = 2.0
    num_steps = 100
    dt = T / num_steps
    tsteps = np.linspace(dt, T, num=num_steps)

    snapshots_new = np.zeros((N, num_steps_coarse))

    for n in range(num_steps_coarse):
        snapshots_new[:, n] = rbf_snapshots[n](parameterNew)

    _, alpha_new, poddim_new, podmodes_new, _ = func.reduced_solution_auto(
        snapshots_new, min_energy_criteria
    )

    podmodes_new = podmodes_new[:, :poddim_new]

    rbf_alpha = Rbf(
        timeGrid, alpha_new.T, function="multiquadric", epsilon=0.01, mode="N-D"
    )

    # --------------------------------------------------------------------------
    #  Result for new time-grid
    # --------------------------------------------------------------------------

    T = 2.0
    num_steps = 200
    dt = T / num_steps
    tsteps = np.linspace(0.02 + dt, 1.99, num=99)
    timeGrid_validate = tsteps

    # True solution for new parameter
    snapshots_new_original = burgers_snap_matrix(parameterNew)

    # Approximate coefficients
    alpha_new_time_grid = rbf_alpha(timeGrid_validate)

    # Approximate solution
    approximate_new_time_grid = np.dot(podmodes_new, alpha_new_time_grid.T)

    error_solution = approximate_new_time_grid - snapshots_new_original

    l2_error = spla.norm(error_solution, axis=0) / spla.norm(
        snapshots_new_original, axis=0
    )

    np.save(f"plots/burgers/Re{parameterNew}/error-values-new-time-Re", l2_error)

    # --------------------------------------------------------------------------
    #  Solution plots
    # --------------------------------------------------------------------------

    fig, ax = plt.subplots(figsize=(8, 6), nrows=1, ncols=1)
    ax.plot(x, snapshots_new_original[:, 4], "b", label="T ($t=0.09$)", linewidth=3)
    ax.plot(
        x,
        approximate_new_time_grid[:, 4],
        "y--",
        label="R ($t=0.09$)",
        markersize=3,
        linewidth=3,
    )
    ax.plot(x, snapshots_new_original[:, 49], "b", label="T ($t=1.01$)", linewidth=3)
    ax.plot(
        x,
        approximate_new_time_grid[:, 49],
        "y--",
        label="R ($t=1.01$)",
        markersize=3,
        linewidth=3,
    )
    ax.plot(x, snapshots_new_original[:, 89], "b", label="T ($t=1.89$)", linewidth=3)
    ax.plot(
        x,
        approximate_new_time_grid[:, 89],
        "y--",
        label="R ($t=1.89$)",
        markersize=3,
        linewidth=3,
    )
    ax.legend(
        loc="upper center",
        bbox_to_anchor=(0.5, 1.25),
        ncol=3,
        fancybox=True,
        shadow=True,
    )
    ax.set_xlabel("Space")
    ax.set_ylabel("Solution")
    ax.set_ylim((0, 0.55))
    plt.tight_layout()
    fig.savefig(f"plots/burgers/Re{parameterNew}/Figure5.4x-solution.png", dpi=200)

    # --------------------------------------------------------------------------

    X_meshgrid, Y_meshgrid = np.meshgrid(x, timeGrid_validate)

    U_mesh_x = snapshots_new_original
    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111, projection="3d")
    surf = ax.plot_surface(
        X_meshgrid.T,
        Y_meshgrid.T,
        U_mesh_x,
        cmap=cm.plasma,
        rstride=1,
        cstride=1,
        shade=True,
        linewidth=0,
        antialiased=False,
    )
    ax.set_xlabel("Space", labelpad=9, rotation=-15)
    ax.set_ylabel("Time", labelpad=6, rotation=50)
    ax.set_zlabel("Solution", labelpad=6)
    plt.title("Ground truth")
    plt.tight_layout()
    plt.savefig(f"plots/burgers/Re{parameterNew}/Figure5.3x-true-solution.png", dpi=200)

    U_mesh_x = approximate_new_time_grid
    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111, projection="3d")
    surf = ax.plot_surface(
        X_meshgrid.T,
        Y_meshgrid.T,
        U_mesh_x,
        cmap=cm.plasma,
        rstride=1,
        cstride=1,
        shade=True,
        linewidth=0,
        antialiased=False,
    )
    ax.set_xlabel("Space", labelpad=9, rotation=-15)
    ax.set_ylabel("Time", labelpad=6, rotation=50)
    ax.set_zlabel("Solution", labelpad=6)
    plt.title("ROM solution")
    plt.tight_layout()
    plt.savefig(
        f"plots/burgers/Re{parameterNew}/Figure5.3x-surrogate-solution.png", dpi=200
    )

    U_mesh_x = approximate_new_time_grid - snapshots_new_original
    fig = plt.figure(figsize=(4, 4.5))
    ax = fig.add_subplot(111)
    contf = ax.contourf(
        X_meshgrid.T, Y_meshgrid.T, U_mesh_x, cmap=cm.plasma, norm=colors.CenteredNorm()
    )
    ax.set_xlabel("Space")
    ax.set_ylabel("Time")
    plt.title("Error", loc="center", pad=65)
    cbar = fig.colorbar(contf, location="top")
    cbar.ax.locator_params(nbins=3)
    plt.tight_layout()
    plt.savefig(f"plots/burgers/Re{parameterNew}/Figure5.3x-error.png", dpi=200)


# --------------------------------------------------------------------------
# Error plots
# --------------------------------------------------------------------------

e1 = np.load("plots/burgers/Re40/error-values-new-time-Re.npy")
e2 = np.load("plots/burgers/Re100/error-values-new-time-Re.npy")
e3 = np.load("plots/burgers/Re350/error-values-new-time-Re.npy")
e4 = np.load("plots/burgers/Re1250/error-values-new-time-Re.npy")
e5 = np.load("plots/burgers/Re3000/error-values-new-time-Re.npy")

# --------------------------------------------------------------------------

fig, ax = plt.subplots(figsize=(6, 4), nrows=1, ncols=1)
ax.plot(timeGrid_validate, e1, "-bo", label="$Re=40$")
ax.plot(timeGrid_validate, e2, "-go", label="$Re=100$")
ax.plot(timeGrid_validate, e3, "-mo", label="$Re=350$")
ax.plot(timeGrid_validate, e4, "-co", label="$Re=1250$")
ax.plot(timeGrid_validate, e5, "-yo", label="$Re=3000$")
ax.plot(np.array([0, 2]), np.array([1e-2, 1e-2]), "r", label="Tolerance")
ax.set_yscale("log")
plt.xlabel("Time")
plt.ylabel("True $L^2$ error")
ax.legend(
    loc="upper center", bbox_to_anchor=(0.5, 1.37), ncol=3, fancybox=True, shadow=True
)
ax.set_yticks([1e-6, 1e-4, 1e-2])
plt.ylim((1e-7, 1e-1))
plt.tight_layout()
fig.savefig("plots/burgers/Figure5.5a.png", dpi=200)

# --------------------------------------------------------------------------

e1 = np.load("plots/burgers/Re40/error-estimate-values-original-time-Re.npy")
e2 = np.load("plots/burgers/Re100/error-estimate-values-original-time-Re.npy")
e3 = np.load("plots/burgers/Re350/error-estimate-values-original-time-Re.npy")
e4 = np.load("plots/burgers/Re1250/error-estimate-values-original-time-Re.npy")
e5 = np.load("plots/burgers/Re3000/error-estimate-values-original-time-Re.npy")

# --------------------------------------------------------------------------

fig, ax = plt.subplots(figsize=(6, 4), nrows=1, ncols=1)
ax.plot(timeGrid, e1, "-bo", label="$Re=40$")
ax.plot(timeGrid, e2, "-go", label="$Re=100$")
ax.plot(timeGrid, e3, "-mo", label="$Re=350$")
ax.plot(timeGrid, e4, "-co", label="$Re=1250$")
ax.plot(timeGrid, e5, "-yo", label="$Re=3000$")
ax.plot(np.array([0, 2]), np.array([1e-2, 1e-2]), "r", label="Tolerance")
ax.set_yscale("log")
plt.xlabel("Time")
plt.ylabel("Estimated $L^2$ error")
ax.legend(
    loc="upper center", bbox_to_anchor=(0.5, 1.37), ncol=3, fancybox=True, shadow=True
)
ax.set_yticks([1e-6, 1e-4, 1e-2])
plt.ylim((1e-7, 1e-1))
plt.tight_layout()
fig.savefig("plots/burgers/Figure5.5b.png", dpi=200)
