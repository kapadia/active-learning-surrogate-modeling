# Active-Learning-Driven Surrogate Modeling for Efficient Simulation of Parametric Nonlinear Systems

This repository contains code and data supporting some of the key numerical experiments for the preprint [1].

Specifically, the codes are a Python implementation for the ActLearn-POD-KSNN surrogate model (Algorithms 1 and 2) in the paper.

The preprint provides a new active learning framework that employs a non-intrusive optimality criterion to sample the parameter domain, in order to efficiently build parametric data-driven surrogate models, while maintaining a user-defined error tolerance for the surrogate's solution approximation.

## Contents and Organization
* There is one numerical experiment, `Burgers_ActLearn.py`, available from the paper [1]. The results generated from the experiments will be saved in the `plots/` folder.
* The folder `scripts/` hold any supporting code files which are used by the main script.

## Dependencies
We have stored all the dependencies along with their versions in `environment.yml`. Create a virtual environment `active-learning` using `conda` and activate it. Execute the following:
 
 ```
 conda env create -f environment.yml
 ```

 ```
 conda activate active-learning
 ```

## Reproducibility 
* After creating and activating the conda environment as stated above, execute `python Burgers_ActLearn.py` in the terminal to reproduce the numerical results from Section 5.1 in the paper [1]. All the results will be saved in the folder `plots/Burgers/`. The expected results are also already provided in `plots/Burgers_backup/`.
* To facilitate an easy connection, the generated plots are named according to the figures in the paper. For instance, you will obtain all the sub-figures from Figure 5.2 to Figure 5.5 with `Figure#.#x` as part of their names.
* Please note that there might be minor formatting differences between the plots obtained via the provided scripts and the post-processed figures present in the paper.

## License
See the [LICENSE](LICENSE) file for license rights and limitations (MIT).

## References
[1]. H. Kapadia, L. Feng, P. Benner, [Active-Learning-Driven Surrogate Modeling for Efficient Simulation of Parametric Nonlinear Systems](https://arxiv.org/abs/2306.06174), arXiv:2306.06174, 2023.

<details><summary>BibTeX</summary><pre>
@TechReport{KapadiaFB23,
    author =      {Kapadia, H. and Feng, L. and Benner, P.},
    title =       {Active-Learning-Driven Surrogate Modeling for Efficient Simulation of Parametric Nonlinear Systems},
    institution = {arXiv},
    year =        {2023},
    type =        {e-print},
    number =      {2306.06174},
    note =        {cs.LG},
    url =         {https://arxiv.org/abs/2306.06174},
    doi =         {10.48550/arXiv.2306.06174}
}
</pre></details>

## Disclaimer
The code has been diligently implemented, ensuring its authenticity. Any bugs or errors that may be present are solely due to oversight and should not be misconstrued as evidence of malicious intent. The author has not manipulated the numerical experiments to substantiate the claims made in the paper. Your assistance would be greatly appreciated if you spot any bugs or errors in the code; kindly report them promptly to the author.

## Support
If you have any further queries, please do not hesitate to contact [Harshit Kapadia](mailto:kapadia@mpi-magdeburg.mpg.de).
