#!/usr/bin/env python
# coding: utf-8

"""
MIT License

Copyright (c) 2023 Harshit Kapadia

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 
Author: Harshit Kapadia
"""


import numpy as np
import scipy.linalg as spla


#######################################################
def norm_l2_rel(snapshot1, snapshot2, rel=1):
    error = snapshot1 - snapshot2
    l2_norm = spla.norm(error, axis=0)
    if rel == 1:
        l2_norm = l2_norm / spla.norm(snapshot1, axis=0)
    elif rel == 2:
        l2_norm = l2_norm / spla.norm(snapshot2, axis=0)
    else:
        sys.exit()
    return l2_norm


#######################################################
def energy_pod(svals, poddim):
    svals_sq = svals**2
    partial_sum = np.sum(svals_sq[0:poddim])
    total_sum = np.sum(svals_sq)
    return partial_sum / total_sum


#######################################################
def podmodes_calc_complete(snapshots):
    leftsvecs, svals, _ = spla.svd(snapshots, full_matrices=False)
    podmodes = leftsvecs
    return podmodes, svals


#######################################################
def reduced_solution_auto(snapshots, energy_criteria):
    podmodes, svals = podmodes_calc_complete(snapshots)
    poddim = 1
    for i in range(svals.shape[-1]):
        energy = energy_pod(svals, i)
        if energy >= 1 - energy_criteria:
            poddim = i + 1
            break
    coefficients = np.dot(podmodes[:, :poddim].T, snapshots)
    reduced_sol = np.dot(podmodes[:, :poddim], coefficients)
    return reduced_sol, coefficients, poddim, podmodes, svals


#######################################################
def reduced_sol_recompute(snapshots, podmodes, poddim):
    coefficients = np.dot(podmodes[:, :poddim].T, snapshots)
    reduced_sol = np.dot(podmodes[:, :poddim], coefficients)
    return reduced_sol, coefficients
